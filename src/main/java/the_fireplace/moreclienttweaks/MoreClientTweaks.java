package the_fireplace.moreclienttweaks;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EnumPlayerModelParts;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.common.config.Property;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.Mod.Instance;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.network.FMLNetworkEvent.ClientConnectedToServerEvent;
import the_fireplace.fireplacecore.FireCoreBaseFile;
import the_fireplace.fireplacecore.config.FCCV;
import the_fireplace.moreclienttweaks.config.MCTConfigValues;
import the_fireplace.moreclienttweaks.event.FMLEvents;
import the_fireplace.moreclienttweaks.proxy.CommonProxy;
/**
 * 
 * @author The_Fireplace
 *
 */
@Mod(modid=MoreClientTweaks.MODID, name=MoreClientTweaks.MODNAME, version=MoreClientTweaks.VERSION, acceptedMinecraftVersions = "1.8", guiFactory = "the_fireplace.moreclienttweaks.config.MCTGuiFactory", canBeDeactivated=true, clientSideOnly=true)
public class MoreClientTweaks {
	@Instance(value = MoreClientTweaks.MODID)
	public static MoreClientTweaks instance;
	public static final String MODID = "moreclienttweaks";
	public static final String MODNAME = "More Client Tweaks";
	public static final String VERSION = "2.0.2.0";

	private static String releaseVersion;
	private static String prereleaseVersion;
	private static final String downloadURL = "http://goo.gl/OSHtIq";
	public static NBTTagCompound update = new NBTTagCompound();

	@SidedProxy(clientSide="the_fireplace.moreclienttweaks.proxy.ClientProxy", serverSide="the_fireplace.moreclienttweaks.proxy.CommonProxy")
	public static CommonProxy proxy;

	public static Configuration config;

	public static Property ENABLECAPETOGGLE_PROPERTY;
	public static Property ENABLEHATTOGGLE_PROPERTY;
	public static Property ENABLECHESTTOGGLE_PROPERTY;
	public static Property ENABLELEFTARMTOGGLE_PROPERTY;
	public static Property ENABLELEFTLEGTOGGLE_PROPERTY;
	public static Property ENABLERIGHTARMTOGGLE_PROPERTY;
	public static Property ENABLERIGHTLEGTOGGLE_PROPERTY;
	public static Property NORMALBRIGHTNESS_PROPERTY;

	public static void syncConfig(){
		MCTConfigValues.SKIN0 = ENABLECAPETOGGLE_PROPERTY.getBoolean();
		MCTConfigValues.SKIN1 = ENABLECHESTTOGGLE_PROPERTY.getBoolean();
		MCTConfigValues.SKIN2 = ENABLELEFTARMTOGGLE_PROPERTY.getBoolean();
		MCTConfigValues.SKIN3 = ENABLERIGHTARMTOGGLE_PROPERTY.getBoolean();
		MCTConfigValues.SKIN4 = ENABLELEFTLEGTOGGLE_PROPERTY.getBoolean();
		MCTConfigValues.SKIN5 = ENABLERIGHTLEGTOGGLE_PROPERTY.getBoolean();
		MCTConfigValues.SKIN6 = ENABLEHATTOGGLE_PROPERTY.getBoolean();
		MCTConfigValues.NORMALBRIGHTNESS = NORMALBRIGHTNESS_PROPERTY.getInt();
		if(config.hasChanged()){
			config.save();
		}
	}
	@EventHandler
	public void PreInit(FMLPreInitializationEvent event){
		FMLCommonHandler.instance().bus().register(new FMLEvents());
		proxy.registerClientEvents();
		proxy.registerRenderers();
		config = new Configuration(event.getSuggestedConfigurationFile());
		config.load();
		config.addCustomCategoryComment(Configuration.CATEGORY_GENERAL, "The enable toggle options control whether those parts of the skin toggle or not when you use the keybind.");
		ENABLECAPETOGGLE_PROPERTY = config.get(Configuration.CATEGORY_GENERAL, MCTConfigValues.SKIN0_NAME, MCTConfigValues.SKIN0_DEFAULT);
		ENABLECAPETOGGLE_PROPERTY.comment = "Enable cape toggle?";
		ENABLECHESTTOGGLE_PROPERTY = config.get(Configuration.CATEGORY_GENERAL, MCTConfigValues.SKIN1_NAME, MCTConfigValues.SKIN1_DEFAULT);
		ENABLECHESTTOGGLE_PROPERTY.comment = "Enable jacket toggle?";
		ENABLELEFTARMTOGGLE_PROPERTY = config.get(Configuration.CATEGORY_GENERAL, MCTConfigValues.SKIN2_NAME, MCTConfigValues.SKIN2_DEFAULT);
		ENABLELEFTARMTOGGLE_PROPERTY.comment = "Enable left sleeve toggle?";
		ENABLERIGHTARMTOGGLE_PROPERTY = config.get(Configuration.CATEGORY_GENERAL, MCTConfigValues.SKIN3_NAME, MCTConfigValues.SKIN3_DEFAULT);
		ENABLERIGHTARMTOGGLE_PROPERTY.comment = "Enable right sleeve toggle?";
		ENABLELEFTLEGTOGGLE_PROPERTY = config.get(Configuration.CATEGORY_GENERAL, MCTConfigValues.SKIN4_NAME, MCTConfigValues.SKIN4_DEFAULT);
		ENABLELEFTLEGTOGGLE_PROPERTY.comment = "Enable left pant leg toggle?";
		ENABLERIGHTLEGTOGGLE_PROPERTY = config.get(Configuration.CATEGORY_GENERAL, MCTConfigValues.SKIN5_NAME, MCTConfigValues.SKIN5_DEFAULT);
		ENABLERIGHTLEGTOGGLE_PROPERTY.comment = "Enable right pant leg toggle?";
		ENABLEHATTOGGLE_PROPERTY = config.get(Configuration.CATEGORY_GENERAL, MCTConfigValues.SKIN6_NAME, MCTConfigValues.SKIN6_DEFAULT);
		ENABLEHATTOGGLE_PROPERTY.comment = "Enable hat toggle?";
		NORMALBRIGHTNESS_PROPERTY = config.get(Configuration.CATEGORY_GENERAL, MCTConfigValues.NORMALBRIGHTNESS_NAME, MCTConfigValues.NORMALBRIGHTNESS_DEFAULT);
		NORMALBRIGHTNESS_PROPERTY.comment = "The normal brightness setting to revert to when high gamma mode is toggled off.";
		syncConfig();
		retriveCurrentVersions();
		FireCoreBaseFile.instance.addUpdateInfo(update, this.MODNAME, this.VERSION, this.prereleaseVersion, this.releaseVersion, this.downloadURL, this.MODID);
	}
	/**
	 * Toggles fullbright
	 */
	public static void toggleFullbright(){
		if(Minecraft.getMinecraft().gameSettings.gammaSetting != 1000000){
			Minecraft.getMinecraft().gameSettings.gammaSetting = 1000000;
		}else{
			Minecraft.getMinecraft().gameSettings.gammaSetting = NORMALBRIGHTNESS_PROPERTY.getInt();
		}
		Minecraft.getMinecraft().gameSettings.saveOptions();
	}
	/**
	 * Toggles the parts of the skin set to toggle in the settings
	 */
	public static void toggleEnabledParts(){
		if(ENABLECAPETOGGLE_PROPERTY.getBoolean()){
			Minecraft.getMinecraft().gameSettings.switchModelPartEnabled(EnumPlayerModelParts.CAPE);
		}
		if(ENABLECHESTTOGGLE_PROPERTY.getBoolean()){
			Minecraft.getMinecraft().gameSettings.switchModelPartEnabled(EnumPlayerModelParts.JACKET);
		}
		if(ENABLELEFTARMTOGGLE_PROPERTY.getBoolean()){
			Minecraft.getMinecraft().gameSettings.switchModelPartEnabled(EnumPlayerModelParts.LEFT_SLEEVE);
		}
		if(ENABLERIGHTARMTOGGLE_PROPERTY.getBoolean()){
			Minecraft.getMinecraft().gameSettings.switchModelPartEnabled(EnumPlayerModelParts.RIGHT_SLEEVE);
		}
		if(ENABLELEFTLEGTOGGLE_PROPERTY.getBoolean()){
			Minecraft.getMinecraft().gameSettings.switchModelPartEnabled(EnumPlayerModelParts.LEFT_PANTS_LEG);
		}
		if(ENABLERIGHTLEGTOGGLE_PROPERTY.getBoolean()){
			Minecraft.getMinecraft().gameSettings.switchModelPartEnabled(EnumPlayerModelParts.RIGHT_PANTS_LEG);
		}
		if(ENABLEHATTOGGLE_PROPERTY.getBoolean()){
			Minecraft.getMinecraft().gameSettings.switchModelPartEnabled(EnumPlayerModelParts.HAT);
		}
		Minecraft.getMinecraft().gameSettings.saveOptions();
		if(FCCV.hasCode("debUg"))
			System.out.println("Skin Parts Toggled");
	}

	/**
	 * This method is client side called when a player joins the game. Both for
	 * a server or a single player world.
	 */
	public static void onPlayerJoinClient(EntityPlayer player,
			ClientConnectedToServerEvent event) {
		if (!prereleaseVersion.equals("")
				&& !releaseVersion.equals("")) {
			switch (FireCoreBaseFile.instance.getUpdateNotification()) {
			case 0:
				if (FireCoreBaseFile.isHigherVersion(VERSION, releaseVersion) && FireCoreBaseFile.isHigherVersion(prereleaseVersion, releaseVersion)) {
					FireCoreBaseFile.sendClientUpdateNotification(player, MODNAME, releaseVersion, downloadURL);
				}else if(FireCoreBaseFile.isHigherVersion(VERSION, prereleaseVersion)){
					FireCoreBaseFile.sendClientUpdateNotification(player, MODNAME, prereleaseVersion, downloadURL);
				}

				break;
			case 1:
				if (FireCoreBaseFile.isHigherVersion(VERSION, releaseVersion)) {
					FireCoreBaseFile.sendClientUpdateNotification(player, MODNAME, releaseVersion, downloadURL);
				}
				break;
			case 2:

				break;
			}
		}
	}

	/**
	 * Retrieves what the latest version is from Dropbox
	 */
	private static void retriveCurrentVersions() {
		try {
			releaseVersion = FireCoreBaseFile.get_content(new URL(
					"https://dl.dropboxusercontent.com/s/93z45l9w9z1urbt/release.version?dl=0")
			.openConnection());

			prereleaseVersion = FireCoreBaseFile.get_content(new URL(
					"https://dl.dropboxusercontent.com/s/pw03x1u2xr9fqgq/prerelease.version?dl=0")
			.openConnection());

		} catch (final MalformedURLException e) {
			System.out.println("Malformed URL Exception");
			releaseVersion = "";
			prereleaseVersion = "";
		} catch (final IOException e) {
			System.out.println("IO Exception");
			releaseVersion = "";
			prereleaseVersion = "";
		}
	}
}
