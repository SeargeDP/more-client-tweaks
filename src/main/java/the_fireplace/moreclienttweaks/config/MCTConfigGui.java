package the_fireplace.moreclienttweaks.config;

import net.minecraft.client.gui.GuiScreen;
import net.minecraftforge.common.config.ConfigElement;
import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.fml.client.config.GuiConfig;
import the_fireplace.moreclienttweaks.MoreClientTweaks;
/**
 * 
 * @author The_Fireplace
 *
 */
public class MCTConfigGui extends GuiConfig{

	public MCTConfigGui(GuiScreen parentScreen) {
		super(parentScreen, new ConfigElement(MoreClientTweaks.config.getCategory(Configuration.CATEGORY_GENERAL)).getChildElements(), MoreClientTweaks.MODID, false,
				false, GuiConfig.getAbridgedConfigPath(MoreClientTweaks.config.toString()));
	}

}
